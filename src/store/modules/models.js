import axios from "axios";

const state = () => ({
  models: [],
  materials: [],
  type: null,
  model: null,
  submodel: null,
  material: null,
  result: null,
  cart: [],
  order: null
})

const actions = {
  getModels ({ commit }) {
    axios.get("https://test.customizer.amigo.ru/api/models")
    .then(function (response) {
      commit('setModels', response.data);
    });
  },
  getMaterials ({ commit }, url) {
    axios.get(url)
    .then(function (response) {
        commit('setMaterials', response.data);
    });
  },
  requestCalculation({ commit }, data) {
    return new Promise((resolve) => {
      axios.post('https://test.customizer.amigo.ru/api/calculate', {
        material_id: data.material,
        model_id: data.model,
        options_values: data.options,
        sizes: data.sizes,
        uuid: data.uuid 
      }).then(res => {
        resolve(res.data);
        commit('resultCalculation', res.data);
      })
    })
  },
  sendOrder({ commit }, items) {
    console.log(items);
    axios.post('', {
      items: items
    }).then(function (response) {
      commit('getOrderResponse', response);
    })
  }
}

const mutations = {
  setModels (state, data) {
    state.models = data;
  },
  setMaterials (state, data) {
    state.materials = data;
  },
  changeType(state, object) {
    state.type = object;
  },
  changeModel(state, object) {
    state.model = object;
  },
  changeSubmodel(state, object) {
    state.submodel = object;
  },
  changeMaterial(state, object) {
    state.material = object;
  },
  resultCalculation(state, result) {
    state.result = result;
  },
  addToCart(state, product) {
    state.cart.push(product);
  },
  removeFromCart(state, index) {
    state.cart.splice(index, 1);
  },
  getOrderResponse(state, response) {
    state.order = response;
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}