import Models from './components/Models.vue'
import Cart from './components/Cart.vue'

const routes = [
  { path: '/', component: Models },
  { path: '/cart', component: Cart }
]

export default routes